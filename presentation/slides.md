---
#==> Theme
theme: default

#==> Cover Customization
background: /media/backgrounds/default-bg-alt.jpeg

# ==> Code Highlighter
highlighter: shiki

#==> Line numbers in code blocks
lineNumbers: true

#==> Fonts
fonts:
    # Normal Font
    sans: 'Open Sans'
    # Serif Font
    serif: 'PT Serif'
    # Code Font
    mono: '"Fira Code", monospace'
    # Provider
    provider: 'none'

#==> Enable Download
download: true

#==> Slide Info
title: Containerization with Docker
hideInToc: true
info: |
    Learn more about Containerization and Docker with this HPC Lab

#==> Persists Drawings?
drawings:
    persist: false
---

<h1 class="text-left z-30 text-white font-light !text-6xl leading-tight !opacity-100">
Containerization<br/>with <span class="font-extrabold">Docker</span>
</h1>


---
src: ./slides/about-me.md
---

---
src: ./slides/points.md
---

---
src: ./slides/docker.md
---

---
src: ./slides/docker-containers.md
---

---
src: ./slides/docker-compose.md
---

---
src: ./slides/docker-run.md
---

---
src: ./slides/docker-file.md
---

---
src: ./slides/file-vs-compose.md
---

---
src: ./slides/benefits.md
---

---
src: ./slides/problems.md
---

---
src: ./slides/docker-security.md
---

---
src: ./slides/links.md
---