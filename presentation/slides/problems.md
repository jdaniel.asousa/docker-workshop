---
layout: center
---

<h1 class="font-light !mb-2 text-center"><strong>Desvantagens</strong></h1>
<p class="!opacity-80 text-center">Infra-estrutura, pessoais, entre outros!</p>

<div class="flex gap-8 flex-wrap items-start justify-center">
<div class="inline-flex flex-col items-center justify-center text-center w-[200px]">
<img src="https://media2.giphy.com/media/9ZOyRdXL7ZVKg/200.webp?cid=ecf05e47fj87dgcu1z54sz9627g2yvq36prf7xneddyb5tso&rid=200.webp&ct=g" class="rounded h-[200px] w-[200px] object-cover" />
<h3 class="mt-2 !mb-0 !dark:text-white !text-[16px]">Perda de dados</h3>
<p class="!mt-1 text-[10px] !leading-tight">Falhas na configuração do container <small>(por ex.)</small></p>
</div>

<div class="inline-flex flex-col items-center justify-center text-center w-[200px]">
<img src="https://media2.giphy.com/media/IPbS5R4fSUl5S/giphy.gif?cid=790b761160338a92dc5e1c7ea8170e1be9e46e79fe129a1d&rid=giphy.webp&ct=g" class="rounded h-[200px] w-[200px] object-cover" />
<h3 class="mt-2 !mb-0 !dark:text-white !text-[16px]">Curva de aprendizagem</h3>
<p class="!mt-1 text-[10px] !leading-tight">Inicialmente acentuada</p>
</div>

<div class="inline-flex flex-col items-center justify-center text-center w-[200px]">
<img src="https://media2.giphy.com/media/KA593kO0JvXMs/giphy.gif?cid=790b7611cf306b12163e543e0cec86e264221771e33d3e24&rid=giphy.webp&ct=g" class="rounded h-[200px] w-[200px] object-cover" />
<h3 class="mt-2 !mb-0 !dark:text-white !text-[16px]"><em>No Graphical Applications</em></h3>
<p class="!mt-1 text-[10px] !leading-tight">Não é "recomendado" para interfaces gráficas</p>
</div>
</div>
