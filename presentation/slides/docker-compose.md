---
# layout: image-right-overlay
# image: '/media/backgrounds/docker-compose-file.jpg'
---

<h1 class="font-extrabold">Docker Compose</h1>

<ul>
    <li><strong>Nome do Ficheiro:</strong> <code>docker-compose.yml</code></li>
    <li><strong>Sintaxe:</strong> <code>yaml</code></li>
</ul>

```yaml {all|5|6|8-10|11-12|all}
version: '3.9'

services:
    database:
        container_name: postgresql
        image: postgres:14.2
        restart: always
        environment:
            - POSTGRES_USER=postgres
            - POSTGRES_PASSWORD=postgres
        ports:
            - 5433:5432
        volumes:
            - ./postgres-data:/var/lib/postgresql/data
```

<div class="absolute top-[35%] right-[5%] max-w-[45%] text-sm">
<h3 class="!text-lg font-bold">Notas:</h3>

-   **`container_name`:** nome para identificar o _container_;
-   **`image`:** imagem a ser utilizada para criar o container;
-   **`environment`:** variáveis de ambiente <small>(varia de acordo com a imagem/projeto)</small>;
-   **`ports`:** portas de execução do projeto;
    -   `5433`: porta para o exterior (utilizada na máquina);
    -   `5432`: porta utilizada pelo serviço no container.

</div>
<div v-click="6" class="rounded bg-teal-900 px-3 py-2 text-white absolute top-22 right-4">
<code class="bg-transparent">docker-compose up -d</code></div>

<!--
[Docker Compose with PostgreSQL](https://levelup.gitconnected.com/creating-and-filling-a-postgres-db-with-docker-compose-e1607f6f882f) -->
