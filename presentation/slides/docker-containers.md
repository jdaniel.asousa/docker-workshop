---
layout: image-right
image: /media/backgrounds/containers.jpg
---

<div class="flex items-center gap-2">
    <logos-docker-icon class="text-6xl" />
    <h1 class="font-light !text-5xl !m-0">
        Docker
    </h1>
</div>

<div class="flex flex-col gap-2 mt-6">
    <h2 class="font-bold">Containers Docker</h2>
    <ul>
        <v-clicks>
        <li>Ambiente isolado;</li>
        <li>Contêm todo o código e dependências necessárias para a sua execução;</li>
        <li>Não contêm a imagem do sistema operativo.</li>
        </v-clicks>
    </ul>
</div>

<!--
O sistema operativo não tem qualquer impacto no conteúdo/funcionamento do container
-->
