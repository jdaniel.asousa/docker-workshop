---
layout: center
---

<div class="flex items-center gap-2">
    <logos-docker-icon class="text-6xl" />
    <h1 class="font-light !text-5xl !m-0">
        Docker
    </h1>
</div>

<div class="grid grid-cols-2 gap-6 mt-4">

<div class="flex flex-col gap-2">
    <h2 class="font-bold">Segurança</h2>
<v-clicks>

-   Apenas garantir as permissões necessárias para o container;
-   Limitar os recursos usados pelo container;
-   Definir volumes como _read-only_;
-   Executar os containers com um utilizador alternativo <small>(evitar o uso do utilizador _root_)</small>

</v-clicks>
</div>

<div class="flex flex-col gap-2">
    <h2 class="font-bold">Boas Práticas</h2>
<v-clicks>

-   Docker sempre atualizado;
-   Garantir que a imagem não possui vulnerabilidades antes da sua utilização;
-   Remover dependências desnecessárias;
-   Não expor o _daemon socket_;
    -   **Exemplo:**
    ```yaml
    volumes:
        - '/var/run/docker.sock:/var/run/docker.sock'
    ```

</v-clicks>
</div>

</div>

<div class="flex flex-col gap-2 mt-4">

</div>

<!--
O sistema operativo não tem qualquer impacto no conteúdo/funcionamento do container
-->
