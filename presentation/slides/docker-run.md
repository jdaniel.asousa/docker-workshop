
<div class="flex gap-1 items-center">
    <h1 class="no-mb font-extrabold"><code>docker run</code></h1>
    <MarkerTerminal />
</div>

```yaml {all|2|3|4|6|7|all}
docker run \
    -d \
    -p 5433:5432 \
    -e POSTGRES_PASSWORD=postgres \
    -v ./postgres.data:/var/lib/postgresql/data \
    --name postgresDb \
    postgres
```

<h3 class="!text-lg font-bold">Notas:</h3>

- **`-d`:** *Detach* a execução do container do terminal;
- **`-p`:** portas de execução do projeto;
  - `5433`: porta para o exterior (utilizada na máquina);
  - `5432`: porta utilizada pelo serviço no container.
- **`-e`:** variáveis de ambiente <small>(varia de acordo com a imagem/projeto)</small>;
- **`--name`:** nome desejado para o container;
- **`postgres`:** imagem desejada para o container.

<!--

-->
