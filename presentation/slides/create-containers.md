---
title: "Containers Docker: Criação"
level: 1
layout: center
class: flex flex-col text-center items-center justify-center
---
<div class="flex justify-center items-center gap-2">
    <logos-docker-icon class="text-6xl" />
    <h1 class="font-light !text-5xl !m-0">
        Docker
    </h1>
</div>

<h2 class="my-4 !font-bold !text-lg">Como criar Containers Docker?</h2>

<div class="grid grid-cols-2 gap-2">
<v-clicks>
    <div class="flex flex-col justify-center items-center gap-2">
        <div class="point">
            <div class="">1</div>
        </div>
        <div class="text-center">
            <h3
                class="!mb-0 !p-0 !leading-tight font-extrabold !text-[1.05rem]"
            >Docker Compose</h3>
            <p class="text-sm !m-0"><code>docker-compose.yml</code></p>
        </div>
    </div>
    <div class="flex flex-col justify-center items-center gap-2">
        <div class="point">
            <div class="">2</div>
        </div>
        <div class="text-center">
            <h3
                class="!mb-0 !p-0 !leading-tight font-extrabold !text-[1.05rem]"
            ><code>docker run</code></h3>
            <p class="text-sm !m-0">via linha de comandos</p>
        </div>
    </div>
</v-clicks>
</div>
