---
layout: center
---

<h1 class="font-light !mb-2 text-center"><strong>Vantagens</strong></h1>

<div class="flex gap-8 flex-wrap items-start justify-center">
<div class="inline-flex flex-col items-center justify-center text-center w-[200px]">
<img src="/media/docker/docker-deploy.jpeg" class="rounded h-[200px] w-[200px] object-cover" />
<h3 class="mt-2 !mb-0 !dark:text-white !text-[16px]">Facilidade nos Deploys</h3>
<p class="!mt-1 text-[10px] !leading-tight">Possibilidade de utilizar em conjunto com <a href="https://www.youtube.com/watch?v=JsOoUrII3EY" target="_blank" class="text-bold">GitHub Actions <lucide:external-link /></a>  ou <a href="https://docs.aws.amazon.com/AmazonECS/latest/userguide/docker-basics.html" target="_blank" class="text-bold">AWS <lucide:external-link /></a></p>
</div>

<div class="inline-flex flex-col items-center justify-center text-center w-[200px]">
<img src="/media/docker/server.png" class="rounded h-[200px] w-[200px] object-cover" />
<h3 class="mt-2 !mb-0 !dark:text-white !text-[16px]">Ambiente Isolado</h3>
<p class="!mt-1 text-[10px] !leading-tight">Apenas contêm o código e respetivas dependências</p>
</div>

<div class="inline-flex flex-col items-center justify-center text-center w-[200px]">
<img src="/media/docker/docker-staging.jpg" class="rounded h-[200px] w-[200px] object-contain bg-white" />
<h3 class="mt-2 !mb-0 !dark:text-white !text-[16px]">Facilidade em todas as fases de desenvolvimento</h3>
<p class="!mt-1 text-[10px] !leading-tight">Em qualquer ambiente e com rapidez!</p>
</div>
</div>