---
layout: image-right
image: /media/backgrounds/containers.jpg
class: "flex flex-col gap-2 mb-4 justify-center"
---

<h1 class="font-light !text-5xl !m-0">
    Links Úteis
</h1>

<ul>
<li><a href="https://www.docker.com/">Docker <small>(Oficial)</small></a></li>
<li><a href="https://docker.events.cube365.net/dockercon/2022" class="font-bold text-brand-yellow hover:text-brand-yellow">Dockercon 2022 <small>(10/05)</small></a></li>
<li><a href="https://hub.docker.com/">Docker Hub</a></li>
<li><a href="https://dockerlabs.collabnix.com/docker/cheatsheet/">Docker Cheat Sheet</a></li>
<li><a href="https://collabnix.com/docker-compose-cheatsheet/">Docker Compose Cheat Sheet</a></li>
<li><a href="https://kapeli.com/cheat_sheets/Dockerfile.docset/Contents/Resources/Documents/index/"><code>Dockerfile</code>: Best Practices & Usage</a></li>
<li><a href="https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04">Install Docker on Ubuntu</a></li>
<li><a href="http://docs.projectatomic.io/container-best-practices/">Containers: Best Practices</a></li>
<li><a href="https://github.com/crosbymichael/Dockerfiles">A collection of Dockerfiles</a></li>
<li><a href="https://csabapalfi.github.io/random-docker-tips">24 random Docker tips</a></li>
</ul>