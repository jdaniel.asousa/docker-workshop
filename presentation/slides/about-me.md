---
title: Sobre Mim
lavel: 1
layout: center
---

<h1 class="font-extrabold !mb-0">Daniel Sousa</h1>
<h3 class="!text-sm"><em>Front-end Developer</em></h3>

<br />

<v-clicks :every="2">
<p class="font-bold">Competências</p>

<div class="grid grid-cols-6 gap-2 w-3/5">
    <IconBox>
        <template v-slot:default>
        <logos-react class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        React
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <logos-angular-icon class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        Angular
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <logos-sass class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        Sass
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <simple-icons-chakraui  class="w-30px h-30px text-[#38B2AC]" />
        </template>
        <template v-slot:title>
        Chakra UI
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <vscode-icons-file-type-tailwind  class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        Tailwind CSS
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <simple-icons-windicss  class="w-30px h-30px text-[rgba(72,176,241,1)]" />
        </template>
        <template v-slot:title>
        WindiCSS
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <logos-material-ui  class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        Material UI
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <logos-bootstrap  class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        Bootstrap
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <logos-nodejs-icon  class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        NodeJS
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <vscode-icons-file-type-nestjs  class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        NestJS
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <logos-javascript  class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        JavaScript
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <logos-typescript-icon  class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        TypeScript
        </template>
    </IconBox>
    <IconBox>
        <template v-slot:default>
        <logos-docker-icon  class="w-30px h-30px" />
        </template>
        <template v-slot:title>
        Docker
        </template>
    </IconBox>
</div>

<div class="mt-16 flex items-center gap-2">

<a href="https://www.linkedin.com/in/daniel-sousa-tutods/" target="_blank" class="inline-flex gap-2 items-center px-2 py-1">
<akar-icons-linkedin-fill />
LinkedIn
</a>

</div>

</v-clicks>

<img src="/media/daniel-sousa.jpg" class="rounded-full w-[200px] h-[200px] object-cover object-top absolute top-[50%] right-12" style="transform: translateY(-50%);"/>
