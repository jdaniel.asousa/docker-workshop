---
layout: image-left
image: '/media/backgrounds/wale.jpg'
class: mt-4
title: "Docker: O que é?"
level: 1
---

<div class="flex items-center gap-2">
    <logos-docker-icon class="text-6xl" />
    <h1 class="font-light !text-5xl !m-0">
        Docker
    </h1>
</div>

<div class="flex flex-col gap-2 mt-6">
    <h2 class="font-bold">O que é?</h2>
    <ul>
<v-clicks>
        <li>Tecnologia responsável pela ativação e uso de <strong>containers Linux</strong>;</li>
        <li>Open-source;</li>
        <li>Criado com o objetivo de facilitar a colocação de uma aplicação disponível sem grande demora;</li>
        </v-clicks>
    </ul>
</div>
