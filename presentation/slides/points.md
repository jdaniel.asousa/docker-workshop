---
layout: image-right
image: '/media/backgrounds/docker-compose-file.jpg'
---

<h1 class="font-extrabold">Estrutura</h1>

<div class="flex flex-col gap-2 justify-between">
  <div class="flex flex-col gap-4">
    <v-clicks>
    <StructurePoint number="1" title="Docker">
        O que é? Para que serve?
    </StructurePoint>
    <StructurePoint number="2" title="Criação de Containers Docker">
        Com <strong>Docker Compose</strong> e <strong><code>docker run</code></strong>
    </StructurePoint>
    <StructurePoint number="3" title="Dockerfile">
        Finalidade vs <strong>Docker Compose</strong>
    </StructurePoint>
    <StructurePoint number="4" title="Vantagens & Desvantagens">
        Principais vantagens e desvantagens
    </StructurePoint>
    <StructurePoint number="5" title="Segurança">
        Algumas das boas práticas a seguir...
    </StructurePoint>
    <StructurePoint number="6" title="Links Úteis">
        Comandos e Guias sobre Docker
    </StructurePoint>
    </v-clicks>
  </div>
</div>
