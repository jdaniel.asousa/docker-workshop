---
layout: center
---

<div class="grid grid-cols-2">

<div class="flex flex-col gap-2">
<h2 class="font-extrabold text-center">Docker File</h2>

<ul>
<li>É um ficheiro;</li>
<li>Possibilita a criação de uma imagem;</li>
<li>Contêm todos os comandos necessários para realizar a execução do projeto;</li>
</ul>

</div>

<div class="flex flex-col gap-2">
<h2 class="font-extrabold text-center">Docker Compose</h2>

<ul>
<li>É uma ferramenta;</li>
<li>
Permite a criação de múltiplos <i>containers</i>;
    <ul>
        <li><strong>Objetivo:</strong> serem executados paralelamente.</li>
    </ul>
</li>
</ul>
</div>

</div>
