---

---

<h1 class="font-extrabold">Docker File</h1>

<ul>
    <li><strong>Nome do Ficheiro:</strong> <code>Dockerfile</code></li>
</ul>

<div class="grid grid-cols-2">

```bash{all|1|3|5-6|8|10|12|14|all}
FROM node:17-alpine3.14

WORKDIR /usr/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install --ignore-scripts

COPY . .

EXPOSE 3030

CMD ["yarn", "slidev", "--remote"]
```

<ul class="pl-4 text-sm">
<li>Dependência(s) necessária(s);</li>
<li>Pasta no container onde ficarão "armazenados" os ficheiros do projeto;</li>
<li>Copiar o ficheiro relativo aos <em>packages</em> utilizados;</li>
<li>Instalar os <em>packages</em> referidos no ficheiro copiado anteriormente;</li>
<li>Copiar os restantes ficheiros do projeto;</li>
<li>Porta que será "exposta" para o container;</li>
<li>Comandos necessários para executar o container;</li>
</ul>

</div>

<div v-click="9" class="rounded bg-teal-900 px-3 py-2 text-white absolute top-22 right-4">
<code class="bg-transparent">docker build &lt;path&gt; -t &lt;image-name&gt;</code></div>
