import { defineWindiSetup } from '@slidev/types';

// extending the builtin windicss configurations
export default defineWindiSetup(() => ({
	shortcuts: {
		'bg-overlay': 'bg-cover fixed bottom-0 -left-[8rem] w-full h-full'
	},
	theme: {
		extend: {
			colors: {
				brand: {
					primary: '#6785C1',
					secondary: '#0080B1',
					'alt-primary': '#0F1C50',
					yellow: '#E6B600',
					red: '#BC4328'
				},
				black75: 'rgba(0,0,0,.75)',
				black65: 'rgba(0,0,0,.65)'
			},
			fontFamily: {
				sans: ['Open Sans', 'sans-serif'],
				sansCondensed: ['Open Sans Condensed'],
				serif: ['PT Serif', 'serif'],
				mono: ['Fira Code', 'monospace'],
				slidev: ['Inter']
			}
		}
	}
}));
