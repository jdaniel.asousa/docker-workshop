<h1 align="center">Docker Workshop</h1>
<p align="center">This repository contains all the code, examples and the presentation I use on this workshop.</p>

<br /><br />

<h2>💡 What you will found?</h2>

You can found the folders below:

```bash
.
├── presentation # Slidev Presentation
└── examples     # Examples used to this workshop
    ├── wordpress
    ├── chakra-weather-app
    ├── postgresql
    └── nestjs-with-graphql
```



<br /><br />

<div align="right" style="margin-top: 50px">
<h3 style="font-weight: 300">
🧑🏻‍💻 About Me
</h3>

<a href="https://github.com/TutoDS" alt="TutoDS">
<img src="https://github.com/tutods.png" alt="Daniel Sousa @TutoDS" width="100px" style="border-radius: 100%">
<br />
 <sub><b>Daniel Sousa @TutoDS</b></sub>
</a>

<div style="margin: 20px 0" />

[linkedin]: https://www.linkedin.com/in/daniel-sousa-tutods/
[github]: https://github.com/tutods
[<img src="https://img.shields.io/badge/LinkedIn%20-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn"/>][linkedin] [<img src="https://img.shields.io/badge/github%20-%23181717.svg?&style=for-the-badge&logo=github&logoColor=white"/>][github]

</div>
