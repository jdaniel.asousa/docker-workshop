<div style="font-weight: 300; margin-top: 30px">
    <h1 align="center" style="font-weight: 300">API com GraphQL e NestJS</h1>
</div>

<br />
<div align="center">
  	<a href="#">
  		<img src="https://img.shields.io/badge/NestJS%20-%2320232a.svg?&style=for-the-badge&logo=nestjs&logoColor=white" alt="NestJS"/>
	</a>
  	<a href="#">
  		<img src="https://img.shields.io/badge/graphql%20-%2320232a.svg?&style=for-the-badge&logo=graphql&logoColor=white" alt="graphql"/>
	</a>
	<a href="#">
		<img src="https://img.shields.io/badge/typescript%20-%23007ACC.svg?&style=for-the-badge&logo=typescript&logoColor=white" alt="TypeScript" />
	</a>
	<a href="#">
		<img src="https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white" alt="VS Code" />
	</a>
</div>

<br />
<br />

<h2 style="font-weight:300">Links com passo a passo:</h2>

<ul>
    <li><a href="https://cdmana.com/2021/08/20210804175044788x.html">CDMana</a> <small>(não foi utilizada a parte do <strong>MongoDB</strong>)</small></li>
    <li><a href="https://dev.to/erezhod/setting-up-a-nestjs-project-with-docker-for-back-end-development-30lg">Dev.to</a></li>
</ul>

<br />

<h2 style="font-weight:300">Como <strong>testar</strong></h2>

Após a criação do container **Docker** é possível aceder localmente ao gestor do **GraphQL** com o url `http://localhost:3000/graphql`.

Já dentro do gestor do **GraphQL** pode testar executar _queries_ e _mutations_, ficam alguns exemplos:

<h3 style="font-weight: 300"><code>Create User</code></h3>

```graphql
mutation {
	createUser(createUserData: { email: "my@email.com", age: 26 }) {
		userId
		email
		age
		isSubscribed
	}
}
```

> Após executar guardar o valor de _userID_ para os restantes testes

<h3 style="font-weight: 300"><code>View User</code></h3>

```graphql
query {
	user(userId: "<userId>") {
		email
		age
		isSubscribed
	}
}
```

> **Nota:** substituir `<userId>` pelo **ID** copiado na criação

<h3 style="font-weight: 300"><code>View Users</code></h3>

```graphql
query {
	users(userIds: ["<userId></userId>"]) {
		userId
		email
		age
	}
}
```

> **Nota:** substituir `<userId>` pelo(s) **ID(s)** copiado(s) na criação
> Nesta query poderá passar vários **IDs** após a criação dos mesmos

<h3 style="font-weight: 300"><code>Update User</code></h3>

```graphql
mutation {
	updateUser(updateUserData: { userId: "<userId>", age: 27 }) {
		userId
		email
		age
	}
}
```

> **Nota:** substituir `<userId>` pelo **ID** copiado na criação

<h3 style="font-weight: 300"><code>Delete User</code></h3>

```graphql
mutation {
	deleteUser(deleteUserData: { userId: "<userId>" }) {
		userId
	}
}
```

> **Nota:** substituir `<userId>` pelo **ID** copiado na criação
