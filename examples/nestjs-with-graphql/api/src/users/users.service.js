"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.UsersService = void 0;
var common_1 = require("@nestjs/common");
var uuid_1 = require("uuid");
var UsersService = /** @class */ (function () {
    function UsersService() {
        this.users = [];
    }
    UsersService.prototype.createUser = function (createUserData) {
        var user = __assign({ userId: (0, uuid_1.v4)() }, createUserData);
        this.users.push(user);
        return user;
    };
    UsersService.prototype.updateUser = function (updateUserData) {
        var user = this.users.find(function (user) { return user.userId === updateUserData.userId; });
        Object.assign(user, updateUserData);
        return user;
    };
    UsersService.prototype.getUser = function (getUserArgs) {
        return this.users.find(function (user) { return user.userId === getUserArgs.userId; });
    };
    UsersService.prototype.getUsers = function (getUserArgs) {
        var _this = this;
        //   return this.users;
        return getUserArgs.userIds.map(function (userId) { return _this.getUser({ userId: userId }); });
    };
    UsersService.prototype.deleteUser = function (deleteUserData) {
        var userIndex = this.users.findIndex(function (user) { return user.userId === deleteUserData.userId; });
        var user = this.users.find(function (user) { return user.userId === deleteUserData.userId; });
        this.users.splice(userIndex);
        return user;
    };
    UsersService = __decorate([
        (0, common_1.Injectable)()
    ], UsersService);
    return UsersService;
}());
exports.UsersService = UsersService;
