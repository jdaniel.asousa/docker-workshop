"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.UsersResolver = void 0;
var graphql_1 = require("@nestjs/graphql");
var user_1 = require("./models/user");
var UsersResolver = /** @class */ (function () {
    function UsersResolver(usersService) {
        this.usersService = usersService;
    }
    UsersResolver.prototype.getUser = function (getUserArgs) {
        return this.usersService.getUser(getUserArgs);
    };
    UsersResolver.prototype.getUsers = function (getUsersArgs) {
        return this.usersService.getUsers(getUsersArgs);
    };
    UsersResolver.prototype.createUser = function (createUserData) {
        return this.usersService.createUser(createUserData);
    };
    UsersResolver.prototype.updateUser = function (updateUserData) {
        return this.usersService.updateUser(updateUserData);
    };
    UsersResolver.prototype.deleteUser = function (deleteUserData) {
        return this.usersService.deleteUser(deleteUserData);
    };
    __decorate([
        (0, graphql_1.Query)(function () { return user_1.User; }, { name: 'user', nullable: true }),
        __param(0, (0, graphql_1.Args)())
    ], UsersResolver.prototype, "getUser");
    __decorate([
        (0, graphql_1.Query)(function () { return [user_1.User]; }, { name: 'users', nullable: 'items' }),
        __param(0, (0, graphql_1.Args)())
    ], UsersResolver.prototype, "getUsers");
    __decorate([
        (0, graphql_1.Mutation)(function () { return user_1.User; }),
        __param(0, (0, graphql_1.Args)('createUserData'))
    ], UsersResolver.prototype, "createUser");
    __decorate([
        (0, graphql_1.Mutation)(function () { return user_1.User; }),
        __param(0, (0, graphql_1.Args)('updateUserData'))
    ], UsersResolver.prototype, "updateUser");
    __decorate([
        (0, graphql_1.Mutation)(function () { return user_1.User; }),
        __param(0, (0, graphql_1.Args)('deleteUserData'))
    ], UsersResolver.prototype, "deleteUser");
    UsersResolver = __decorate([
        (0, graphql_1.Resolver)(function () { return user_1.User; })
    ], UsersResolver);
    return UsersResolver;
}());
exports.UsersResolver = UsersResolver;
