# Dev Environment
FROM node:alpine AS development

# Container Dir
WORKDIR /usr/src/app

# Copy package.json (to install all the packages)
COPY package*.json ./

# Install the packages (on the package.json)
RUN npm install

# Copy all files
COPY . .

# Run the build command
RUN npm run build

# Prod Environment
FROM node:alpine AS production

# Set environment variables
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

# Container Dir
WORKDIR /usr/src/app

# Copy the package.json (to install all the packages)
COPY package*.json ./

# Install all the packages (only the production packages)
RUN npm install --only=production

# Copy all files
COPY . .

# Copy dist folder (build folder) from development
COPY --from=development /usr/src/app/dist ./dist

# Command to run the server
CMD ["node", "dist/main"]