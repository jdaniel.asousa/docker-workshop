<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'wordpress' );

/** Database password */
define( 'DB_PASSWORD', 'wordpress' );

/** Database hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-w8i#O%ba?GKcMf0~H$muK/ZwdKm6?D{9>Z!X@A|yqE}(Z[a(h=:Wj%m !$d6>O/');
define('SECURE_AUTH_KEY',  '|zy:<+vW@k67L2%p}-Nyw/7gGFbvL]Ykz9rQjF&.?A/7mA)vm_j/Vzj@9Ag:=jGh');
define('LOGGED_IN_KEY',    'r>c5a-a8_Rs{T0 EJI nUB5|,+B;!(2CPPeHr979IvyxSHmwN+[iZDFFV^Q~fH3+');
define('NONCE_KEY',        'n0n{{1P,tL}6dXmBTpK+3<OBuUk+<A:j|Duv<JQ4{p2*$*<2NwzaV1Xv0DkXq!e{');
define('AUTH_SALT',        'Mg+61Akp^!1.RC(WY`AGg|o3Jk`BGeoDbz.T$O5y_M(RiPt,SU4IZHe|8I99bK{=');
define('SECURE_AUTH_SALT', 'my$. e%1 #18L-[Sf)tf{-Y8UIrbpGtw!)!u i)MqO|WLWyWMqxK{W fwUjPN!oQ');
define('LOGGED_IN_SALT',   'H/+#Rt!?IXdvxCy`e$,QH4;s~vY4Wxxx2o8.-.Ni.~$Xt0VPU`czw ck#e=%gG- ');
define('NONCE_SALT',       ' )g9,2AQSU(b10U]e:?|4mf7U@`LRGoRcJS6:>5y7;;pIs@(<zIOF|AgM]+]Tsz+');


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
